package com.example.animelist.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;

import com.example.animelist.R;
import com.example.animelist.adapters.MyAdapter;
import com.example.animelist.models.Anime;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView listView;
    private MenuItem listMenu;
    private MenuItem gridMenu;
    private GridView gridView;
    private MyAdapter listAdapter;
    private MyAdapter gridAdapter;
    public static ArrayList<Anime> animeList = new ArrayList<Anime>();
    public static int animePosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.activityMainListView);
        gridView = findViewById(R.id.activityMainGridView);

        if (animeList.isEmpty()){
            loadAnimes();
        }

        listAdapter = new MyAdapter(this, R.layout.types_of_anime, animeList);
        gridAdapter = new MyAdapter(this, R.layout.grid_anime, animeList);


        listView.setAdapter(listAdapter);
        gridView.setAdapter(gridAdapter);

        listAdapter.notifyDataSetChanged();
        gridAdapter.notifyDataSetChanged();

        registerForContextMenu(listView);
        registerForContextMenu(gridView);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent=new Intent(MainActivity.this, EditAnime.class);
                MainActivity.animePosition = position;
                startActivity(intent);
            }
        });
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent=new Intent(MainActivity.this, EditAnime.class);
                MainActivity.animePosition = position;
                startActivity(intent);
            }
        });

    }

    public void loadAnimes(){
        animeList = new ArrayList<Anime>() {{
            add(new Anime("One Piece", "acción, aventuras", "shonen", 1997));
            add(new Anime("Berserk", "acción, fantasía oscura, gore", "seinen", 1989));
            add(new Anime("Pokémon", "aventuras, maltrato animal", "kodomo", 1997));
            add(new Anime("Sailor Moon", "acción, romance, magical girls", "shoujo", 1997));
            add(new Anime("Slam Dunk", "spokon, comedia", "shonen", 1990));
        }};
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        menu.setHeaderTitle(MainActivity.animeList.get(info.position).getTitle());

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.delete_item:
                MainActivity.animeList.remove(info.position);
                listAdapter.notifyDataSetChanged();
                gridAdapter.notifyDataSetChanged();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_bar_menu, menu);
        listMenu = menu.findItem(R.id.changeViewList);
        gridMenu = menu.findItem(R.id.changeViewGrid);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.addItem:
                Intent intent = new Intent(MainActivity.this, AddAnime.class);
                startActivity(intent);
            case R.id.changeViewList:
                listMenu.setVisible(false);
                listView.setVisibility(View.VISIBLE);
                gridMenu.setVisible(true);
                gridView.setVisibility(View.INVISIBLE);
                return true;
            case R.id.changeViewGrid:
                listMenu.setVisible(true);
                listView.setVisibility(View.INVISIBLE);
                gridMenu.setVisible(false);
                gridView.setVisibility(View.VISIBLE);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}