package com.example.animelist.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.animelist.R;
import com.example.animelist.models.Anime;

public class EditAnime extends AppCompatActivity {

    private EditText title, category, releaseYear;
    private Button editAnime;
    private ImageView imageView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_anime);
        final Intent intent = getIntent();

        title = findViewById(R.id.editAnimeTitleET);
        category = findViewById(R.id.editAnimeCategoryET);
        releaseYear = findViewById(R.id.editAnimeReleaseYearET);
        editAnime = findViewById(R.id.buttonEditAnime);
        imageView = findViewById(R.id.imageViewEditAnime);


        Anime anime = MainActivity.animeList.get(MainActivity.animePosition);

        final String animeGenre = anime.getGenre();

        switch (animeGenre){
            case "shonen":
            case "shounen":
                imageView.setImageResource(R.mipmap.ic_shounen_foreground);
                break;
            case "shojo":
            case "shoujo":
                imageView.setImageResource(R.mipmap.ic_shoujo_foreground);
                break;
            case "seinen":
                imageView.setImageResource(R.mipmap.ic_seinen_foreground);
                break;
            case "kodomo":
                imageView.setImageResource(R.mipmap.ic_kodomo_foreground);
                break;
        }


        title.setText(anime.getTitle());
        category.setText(anime.getCategory());
        String originalRun = anime.getOriginalRun() + "";
        releaseYear.setText(originalRun);


        editAnime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String newTitle = title.getText().toString();
                final String newCategory = category.getText().toString();
                final int newReleaseYear = Integer.parseInt(releaseYear.getText().toString());

                Anime editAnime = new Anime(newTitle, newCategory, animeGenre, newReleaseYear);
                MainActivity.animeList.set(MainActivity.animePosition, editAnime);

                Intent getBack =new Intent(EditAnime.this, MainActivity.class);
                startActivity(getBack);
            }
        });

    }
}
