package com.example.animelist.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.text.Editable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.animelist.R;
import com.example.animelist.models.Anime;

public class AddAnime extends AppCompatActivity {

    private EditText animeTitle, animeCategory, animeReleaseYear;
    private ImageView imageView;
    private Spinner spinner;
    private Button sendAnime;
    private String animeGenre;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_anime);

        Bundle bundle = getIntent().getExtras();

        spinner = (Spinner) findViewById(R.id.spinnerAnimeGenre);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.genresOfAnime, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        animeTitle = findViewById(R.id.animeTitleET);
        animeCategory = findViewById(R.id.animeCategoryET);
        animeReleaseYear = findViewById(R.id.animeReleaseYearET);
        spinner = findViewById(R.id.spinnerAnimeGenre);
        sendAnime = findViewById(R.id.newAnime);
        imageView = findViewById(R.id.imageViewAnimeGenre);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int myPosition = spinner.getSelectedItemPosition();
                switch (myPosition){
                    case 0:
                        imageView.setImageResource(R.mipmap.ic_shounen_foreground);
                        animeGenre = "shonen";
                        break;
                    case 1:
                        imageView.setImageResource(R.mipmap.ic_shoujo_foreground);
                        animeGenre = "shojo";
                        break;
                    case 2:
                        imageView.setImageResource(R.mipmap.ic_seinen_foreground);
                        animeGenre = "seinen";
                        break;
                    case 3:
                        imageView.setImageResource(R.mipmap.ic_kodomo_foreground);
                        animeGenre = "kodomo";
                        break;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        sendAnime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (animeTitle.getText().toString().isEmpty()){
                    Toast.makeText(AddAnime.this, "Es necesario rellenar todos los campos", Toast.LENGTH_SHORT).show();
                }else {
                    if (animeCategory.getText().toString().isEmpty()){
                        Toast.makeText(AddAnime.this, "Es necesario rellenar todos los campos", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        if (animeReleaseYear.getText().toString().isEmpty()){
                            Toast.makeText(AddAnime.this, "Es necesario rellenar todos los campos", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            String title = animeTitle.getText().toString();
                            String category = animeCategory.getText().toString();
                            String releasedYearString = animeReleaseYear.getText().toString();
                            int releasedYear = Integer.parseInt(releasedYearString);

                            Anime newAnime = new Anime(title, category, animeGenre, releasedYear);

                            MainActivity.animeList.add(newAnime);

                            Intent intent=new Intent(AddAnime.this, MainActivity.class);
                            startActivity(intent);
                        }
                    }
                }
            }
        });






    }
}
