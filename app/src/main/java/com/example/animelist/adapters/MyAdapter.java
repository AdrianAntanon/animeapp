package com.example.animelist.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.animelist.R;
import com.example.animelist.models.Anime;

import java.util.List;

public class MyAdapter extends BaseAdapter {

    private Context context;
    private int layout;
    private List<Anime> listOfAnime;

    public MyAdapter(Context context, int layout, List<Anime> listOfAnime) {
        this.context = context;
        this.layout = layout;
        this.listOfAnime = listOfAnime;
    }

    @Override
    public int getCount() {
        return this.listOfAnime.size();
    }

    @Override
    public Object getItem(int position) {
        return this.listOfAnime.get(position);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(this.context);
            convertView = inflater.inflate(this.layout, null);

            holder = new ViewHolder();

            holder.nameTextView = convertView.findViewById(R.id.typeOfAnimeTextView);
            holder.imageView = convertView.findViewById(R.id.typeOfAnimeImageView);

            convertView.setTag(holder);
        } else {

            holder = (ViewHolder) convertView.getTag();
        }

        Anime anime = (Anime) getItem(position);
        String animeInfo = anime.getInfoAnime();

        holder.nameTextView.setText(animeInfo);
        holder.imageView.setImageResource(anime.getImage());

        return convertView;


    }

    static class ViewHolder {
        private TextView nameTextView;
        private ImageView imageView;
    }
}
